/**
 * Created by Алина on 15.01.2022.
 */

// Теоретический вопрос
    // 1. Опишите своими словами, как Вы понимаете, что такое Document Object Model (DOM).
    // DOM - это модель страницы, которая представляет эту страницу в виде отдельных объектов,
    //  с которыми можно производить различные манипуляции, как-то их изменять.

let currentArray = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];
let parentElem = document.body;

function getList(array, docEl) {
    let newList = document.createElement("ol");
    docEl.prepend(newList);
    let newArray = array.map((item) => {
        return `<li>${item}</li>`
    })
    newArray.forEach((item) => {
        newList.insertAdjacentHTML("beforeend", item)
    })
}

getList(currentArray, parentElem);

